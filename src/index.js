import {  reducer } from './reducer';
import { titleChanged } from './actions';

var riot = require('riot');
var redux = require('redux');


// Requite tag files
require('./tags/app.tag');
require('./tags/form-header.tag');
require('./tags/form.tag');


var reduxStore = redux.createStore(reducer);

document.addEventListener('DOMContentLoaded', () => {
  riot.mount('app', {
    store: reduxStore,
    actions: {
      titleChanged
    }
  });
} );
