import * as event from './eventTypes';

// Root reducer
export function reducer(state = { 'title' : 'World'}, action) {
  console.log(action)
  switch (action.type) {
    case event.TITLE_CHANGED:
      // return a new state object, don't reuse the original state
      let newState = Object.assign({}, state, {title: action.title});
      console.log("reducer returning the new state: ", newState);
      return newState;
      break;

    default:
      // Always return the unmodifed state as passed if we do not recognize
      // the event type.
      console.log('unknown event type; just return the default')
      return state;
  }
}
