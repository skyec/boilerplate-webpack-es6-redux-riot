<app>

  <form-header title={setTitle()}></form-header>
  <sample-form changetitle={bindChangeTitle}></sample-form>

  <script>
    this.bindChangeTitle = (newTitle) => {
      console.log('bindChangeTitle');
      let store = this.opts.store
      store.dispatch(this.opts.actions.titleChanged(newTitle));
    }

    setTitle() {
      return this.opts.store.getState().title
    }

    // watch for updates in the root tag so that they are propigated all
    // the way down
    this.opts.store.subscribe(function() {
      this.update()
    }.bind(this))

  </script>
</app>
