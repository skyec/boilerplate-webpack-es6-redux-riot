import * as event from './eventTypes.js';

export function titleChanged (newTitle) {
  return {
    type: event.TITLE_CHANGED,
    title: newTitle
  }
}
