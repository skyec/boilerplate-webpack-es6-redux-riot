
import expect from 'expect'
import { reducer } from '../src/reducer';
import * as event from '../src/eventTypes';

describe('hello reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual({
        title: 'World',
    })
  })

  it('should handle TITLE_CHANGED', () => {
    expect(
      reducer(undefined, {
        type: event.TITLE_CHANGED,
        title: 'Bob'
      })
    ).toEqual({
      title: 'Bob',
    })
  })
})
